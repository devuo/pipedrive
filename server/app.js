import express from 'express';
import session from 'express-session';
import path from 'path';

export const app = express();

app.use('/public', express.static(path.join(__dirname, '..', 'dist', 'public')));

app.use(session({
  secret: 'a-very-secret-secret', // Evidently, were this an actual app, this would not be hardcoded
  resave: false,
  saveUninitialized: true
}));
