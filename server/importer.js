import csv from 'fast-csv';
import fs from 'fs';
import miss from 'mississippi';
import stream from 'stream';
import readline from 'readline';

import * as people from './people';

/**
 * Importer imports people from a structured CSV file
 */
export class Importer {

  /**
   * Constructor
   *
   * @param {string} filePath
   *  The valid file path for the .csv containing the persons to be imported
   * @param {function} progressCallback
   *  Callback executed for each person imported
   * @param {function} finishCallback
   *  Callback executed when the import was finished or aborted
   */
  constructor(filePath, progressCallback, finishCallback) {
    this._collection = people.collection;
    this._filePath = filePath;
    this._progressCallback = progressCallback;
    this._finishCallback = finishCallback;

    this._started = undefined;
    this._imported = 0;
    this._total = 0;
  }

  /**
   * Executes the people importer
   *
   * Can only be executed once, all further executions will be ignored.
   */
  execute() {
  
    // If execute has already been invoked, bailout
    if (this._started) {
      return;
    }
    
    // Purge the data contained in collection
    this._collection.clear();
  
    // Set the import initial state
    this._started = new Date();
    this._imported = 0;
    this._total = 0;
  
    // Then count the lines to be imported and import the CSV file
    this._count(() => this._import());
  }
  
  /**
   * Counts the total number of lines in the file to be imported
   *
   * @param {Function} cb
   *  The callback to be executed once the total lines in the file have been
   *  counted.
   * @private
   */
  _count(cb) {
    let rl = readline.createInterface({
      input: fs.createReadStream(this._filePath),
    });
    
    rl.on('line', () => this._total++);
    rl.on('close', () => cb());
  }
  
  /**
   * Stream imports the file to the database collection
   *
   * @private
   */
  _import() {
    let stream = fs.createReadStream(this._filePath)
      .pipe(csv())
      .pipe(this._streamTransformer())
      .pipe(this._streamWriter())
    ;
  
    this._streamFinisher(stream);
  }

  /**
   * Transforms a CSV array to a structured 'person' object
   *
   * @return {stream.Transform}
   * @private
   */
  _streamTransformer() {
    return miss.through.obj((chunk, enc, cb) => {
      return cb(null, {
        id: chunk[0],
        name: chunk[1],
        age: chunk[2],
        address: chunk[3],
        team: chunk[4]
      });
    });
  }

  /**
   * Inserts a person into the database collection
   *
   * @return {stream.Writable}
   * @private
   */
  _streamWriter() {
    return miss.to.obj((chunk, enc, cb) => {
      this._collection.insert(chunk);
      this._imported++;

      this._progressCallback({
        imported: this._imported,
        total: this._total
      });

      cb();
    });
  }

  /**
   * Handles the import stream finish
   *
   * @param {stream.Writable} stream
   * @private
   */
  _streamFinisher(stream) {
    miss.finished(stream, (err) => {
      if (err) {
        return;
      }

      let finished = new Date();

      this._finishCallback({
        total: this._total,
        imported: this._imported,
        started: this._started,
        finished: finished,
        elapsed: finished - this._started
      });
    });
  }
}
