import emojic from 'emojic';
import {app} from './app';
import './router';

app.listen('8080', () => {
  console.log(emojic.earthAfrica + '  http://localhost:8080')
});
