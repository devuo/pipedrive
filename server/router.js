import bodyParser from 'body-parser';
import multer from 'multer';
import path from 'path';

import {app} from './app';
import {Importer} from './importer';
import * as people from './people';

/**
 * Root endpoint
 */
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '..', 'client', 'index.html'));
});

/**
 * People search endpoint
 *
 * Returns an list of people that match the post'ed query string, either in
 * their name or address fields.
 */
app.post('/search', bodyParser.json(), (req, res) => {
  let query = req.body.query || '';

  if (query.length) {
    res.json(people.find(query));
    return;
  }

  res.json([]);
});

/**
 * People import endpoint
 *
 * While import is underway, the controller makes available via the user session
 * the current status of the import. Clients can poll the /import-status
 * endpoint at an interval until this endpoint responds to fetch an updated
 * status of the import.
 *
 * Currently no validation is being done on the submitted CSV data structure,
 * so please (?) :^) don't send adversarial input as it will fail.
 */
app.post('/import', multer({ dest: 'uploads/' }).single('uploadField'), (req, res) => {
  
  // If there's an import running for the current user session, abort now
  if (req.session.importStatus && req.session.importStatus.state == 'running') {
    res.sendStatus(412); // 412: Precondition Failed
    return;
  }
  
  // Number of items that have to be processed before the status import
  // is updated in the user session.
  const STATUS_REPORT_THRESHOLD = 10000;

  // Set the state of the import status in the user session as running
  req.session.importStatus = {
    state: 'running',
    imported: 0,
    total: 0
  };

  // Declare the importer on-progress & on-finish callbacks
  let onProgress = (status) => {
    if (status.imported % STATUS_REPORT_THRESHOLD == 0) {
      req.session.importStatus.imported = status.imported;
      req.session.importStatus.total = status.total;
      req.session.save();
    }
  };

  let onFinish = (status) => {
    req.session.importStatus = null;
    res.json(status);
  };
  
  (new Importer(req.file.path, onProgress, onFinish)).execute();
});

/**
 * People import status endpoint
 *
 * Returns the current status of an ongoing import if it exists
 */
app.get('/import-status', (req, res) => {
  res.json(req.session.importStatus || { state: 'stopped', imported: null, total: null });
});
