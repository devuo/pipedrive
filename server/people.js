import Loki from 'lokijs';

const db = new Loki('app.db');

/**
 * The collection of people
 *
 * @type {Collection}
 */
export const collection = db.addCollection('people');

/**
 * Finds people matching the given query
 *
 * @param query
 *  The query to find matching people with
 * @return {*|Array}
 *  A list of people that match the query
 */
export function find(query) {
  let regex = new RegExp(query, 'i');
  
  return collection.chain()
    .find({
      '$or': [
        { 'name': { '$regex': regex } },
        { 'address': { '$regex': regex } },
        { 'team': { '$regex': regex } },
        { 'age': { '$regex': regex } }
      ]
    })
    .simplesort('name')
    .limit(20)
    .data()
    .map((row) => {
      return {
        id: parseInt(row.id),
        name: row.name,
        age: parseInt(row.age),
        address: row.address,
        team: row.team
      }
    })
  ;
}
