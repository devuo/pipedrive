import Vue from 'vue';

const ENTITY_MAP = {
  "&": "&amp;",
  "<": "&lt;",
  ">": "&gt;",
  '"': '&quot;',
  "'": '&#39;',
  "/": '&#x2F;'
};

function escape(string) {
  return String(string).replace(/[&<>"'\/]/g, (s) => {
    return ENTITY_MAP[s];
  });
}

Vue.filter('highlight', (text, query) => {
  let escapedText = escape(text);
  let escapedQuery = escape(query).trim();
  
  if (!escapedQuery.length) {
    return escapedText;
  }
  
  let regex = new RegExp(`(${escapedQuery})`, 'ig');
  return escapedText.replace(regex, `<span class="filter--highlight">$1</span>`);
});
