# Pipedrive Task

Pipedrive Fullstack development task assignment.


## Objective
The objective of this task was to implement a server and a client application
where a user can upload a [CSV file](docs/testdata.csv) containing a list of people and could
subsequently search for those people in the client application. Besides this, 
the task required that:

1. The user must be able to see a progress bar while the file is
   uploaded/processed.
2. The user must see results be filtered in real time while he types in the
   search box.
3. The user must be able to click on a result and see the full details of the
   person he clicked on.
4. This must be done without any page refreshes (read: a single page application).

#### Components
The task was solved by using the following components:

- **Frontend**: Browserify, Vue 2 and SASS
- **Backend**: Express, LokiJS, FastCSV and Node Streams

## Starting the Application

Start the application by running the following commands:
```
npm install
npm start
```

The web server will then be accessible via [http://localhost:8080](http://localhost:8080])

## Development Mode

You can run the application in development mode, with server auto-reload and
HMR (hot module reload) for the client changes by running the following
commands:

```
npm install
npm run dev
```

## Screenshots
![Initial screen](docs/1-start.png)
![Upload in progress](docs/2-upload.png)
![Searching for results](docs/3-search.png)
![Expanding results](docs/4-expand.png)

## Notes

1. Data is only persisted in memory (given that no requirement said it
   had to be persisted).
2. Any data that has been previously sent from a CSV will be purged from memory
   when importing a new CSV file.
3. There's currently no validation being done to check the CSV structure, and
   the system optimistically assumes only valid data will be sent.
